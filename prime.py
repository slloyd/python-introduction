# -*- coding: utf-8 -*-
"""
This script checks if a number is a prime.
"""

# Define a function to check if a number is a prime
def is_a_prime(number):
    """
    returns True if number is a prime, otherwise returns False.

    Example:

    >>> import is_a_prime
    >>> is_a_prime(1234567)
    False
    >>> is_a_prime(1299827)
    True
    """

    for i in range(2, number):
        if (number % i) == 0:
            return False
    return True

# Main function
def main():
    """
    main function to execute when this script is called directly
    """
    import sys

    try:
        input_number = sys.argv[1]
    except IndexError:
        input_number = input("Please enter a number: ")

    if is_a_prime(int(input_number)):
        print(input_number, 'is a prime')
    else:
        print(input_number, 'is not a prime')

# Run main function if we run this script directly (will be ignored if imported
# from another script).
if __name__ == '__main__':
    main()
