import datetime
import argparse
import requests
import matplotlib.pyplot as plt
from requests_html import HTMLSession

#####
# DEFAULT Values
####

# Search Twitter posts for this word
SEARCH_WORD = 'Brexit'

# How far into the past to search
SEARCH_DAYS = 7

# Width of date range
SEARCH_DAY_STEP = 1

# Stop parsing tweets after this many
MAX_TWEETS = 200

# Print extra information
VERBOSE = False

# Tweaks for rendering the Twitter webpage
SCROLLDOWN = 40
SLEEP = 0.2
WAIT = 2


def calc_dates(days, step):
    """
    Calculate the start date and end date for the
    twitter search.
    """
    dates = []
    today = datetime.date.today()
    for delta_days in range(-1 * days, 0, step):
        start_delta = datetime.timedelta(days=delta_days)
        stop_delta = datetime.timedelta(days=delta_days+step)
        start_date = today + start_delta
        stop_date = today + stop_delta
        dates.append((start_date, stop_date))
    return dates


def search_twitter(search_word, start_date, stop_date):
    """
    Open a session to the twitter search page and return the response.
    """
    url = 'https://twitter.com/search'
    session = HTMLSession()
    r = session.get(url, params={'f': 'news', 'q': f'{search_word} since:{start_date} until:{stop_date}'})
    if VERBOSE is True:
        print(f'sent request: {r.url}')
    return r


def render_tweets(r):
    """
    Render the java-script on the twitter page.
    """
    if VERBOSE is True:
        print(f'Waiting {WAIT} seconds for page to load, then pressing PageDown {SCROLLDOWN} times every {SLEEP} seconds')
    r.html.render(wait=WAIT, scrolldown=SCROLLDOWN, sleep=SLEEP)
    tweets = r.html.find('div.tweet')
    if VERBOSE is True:
        print(f'Saved {len(tweets)} tweets')
    return tweets


def parse_tweet(tweet):
    """
    Take the html from a single tweet and parse it.
    """
    tweet_text = tweet.find('.tweet-text', first=True, clean=True).text
    all_links = tweet.absolute_links

    # keep only shortened links and resolve them
    short_links = {link for link in all_links if "https://t.co/" in link}
    resolved_links = set()
    for short_link in short_links:
        try:
            resolved_url = requests.get(short_link).url
            resolved_links.add(resolved_url)
        except:
            pass
    return (tweet_text, resolved_links)


def get_text_from_url(url):
    """
    Follow urls in tweet and get
    text from the target page.
    """
    if VERBOSE is True:
        print('==>', url)
    session = HTMLSession()
    try:
        r = session.get(url)
        return r.html.text
    except:
        return None

def plot_results(results):
    """
    Plot the search results.
    """
    fig, ax = plt.subplots()
    for item in results.keys():
        dates, counts = [], []
        for date, count in results[item]:
            dates.append(date)
            counts.append(count)
        plt.plot_date(dates, counts, linestyle='solid', label=item)
    ax.xaxis.set_tick_params(rotation=30, labelsize=10)
    plt.legend(loc='upper left')
    plt.ylabel('Normalised count per 100 tweets')
    ax.set_title('Popularity on Twitter')
    plt.show()


def main():
    parser = argparse.ArgumentParser(description='Count keywords of a twitter search.')
    parser.add_argument('keywords', metavar='keywords', type=str, nargs='+',
                        help='scan tweets for this/these keyword(s)')
    parser.add_argument('-s', '--search', dest='search_word', action='store',
                        type=str, default=SEARCH_WORD,
                        help=f'search twitter for SEARCH_WORD (default: {SEARCH_WORD})')
    parser.add_argument('-d', '--days', dest='search_days', action='store',
                        type=int, default=SEARCH_DAYS,
                        help=f'start searching SEARCH_DAYS ago (default: {SEARCH_DAYS})')
    parser.add_argument('-i', '--interval', dest='step', action='store',
                        type=int, default=SEARCH_DAY_STEP,
                        help=f'timewindow width of each search interval in days \
                        (default: {SEARCH_DAY_STEP})')
    parser.add_argument('-m', '--max-tweets', dest='max_tweets', action='store',
                        type=int, default=MAX_TWEETS,
                        help=f'max number of tweets per search to analyse (default: {MAX_TWEETS})')
    parser.add_argument('-f', '--follow-urls', dest='follow_urls', action='store_true',
                        help=f'Follow urls in tweets and count keywords from there too')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                        help=f'Print extra output')

    args = parser.parse_args()

    global VERBOSE
    VERBOSE = args.verbose

    # calculate start and stop dates for the twitter search
    # and save them as a list of tuples
    dates = calc_dates(args.search_days, args.step)

    # save search results in this dictionary
    count_results = {}

    for start_date, stop_date in dates:

        # all text goes into one string
        # and all urls into one set
        text = ""
        urls = set()

        # request the twitter search page using date range and search word
        r = search_twitter(args.search_word, start_date, stop_date)

        # render the twitter webpage and scroll down
        tweets = render_tweets(r)

        # remove extra tweets if we have too many before processing them further
        if len(tweets) > args.max_tweets:
            tweets = tweets[:args.max_tweets]

        # parse tweets
        for i, tweet in enumerate(tweets):
            if VERBOSE is True:
                print(f'parsing tweet #{i+1}/{len(tweets)}')
            tweet_text, tweet_urls = parse_tweet(tweet)
            text += tweet_text
            urls = urls.union(tweet_urls)

        # get text from the linked websites in tweet_links
        if args.follow_urls is True:
            if VERBOSE is True:
                print('fetching content from urls')
            for url in urls:
                url_text = get_text_from_url(url)
                text += url_text

        for keyword in args.keywords:
            # count the number of occurences and normalise
            count = float(text.count(keyword))
            count /= len(tweets) / 100

            # append to results dictionary
            try:
                count_results[keyword].append((stop_date, count))
            except KeyError:
                count_results[keyword] = [(stop_date, count)]

    # plot results
    plot_results(count_results)

if __name__ == "__main__":
    main()
